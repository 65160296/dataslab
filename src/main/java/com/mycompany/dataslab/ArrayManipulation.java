/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dataslab;

/**
 *
 * @author Tncpop
 */
public class ArrayManipulation {
    public static void main(String[] args) {
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = {1.5, 2.7, 3.2, 4.9};

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);

        }
        for (String name : names) {
            System.out.println(name);
        }
        for (int i = 0; i < values.length; i++) {

            System.out.println(values[i] + i);

        }
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        System.out.println("Sum of all elements in the number array is :" + sum);

        double max = 0;
        for (int i = 0; i < values.length; i++) {
            if (values[i] > max) {
                max = values[i];
            }

        }
        System.out.println("Max number in array is :" + max);

        String[] reversedNames = new String[names.length];
        for (int i = 0; i < names.length; i++) {
            reversedNames[i] = names[names.length - 1 - i];
        }
        for (String name : reversedNames) {
            System.out.println(name);
        }
    }

}


